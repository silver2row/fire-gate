//--------P9_41_42_IOPADS
P9_41_42_IOPADS P9_41_42_IOPADS_0(
        // Inputs
        .GPIO_OE  ( GPIO_OE_const_net_3 ),
        .GPIO_OUT ( GPIO_OUT_const_net_3 ),
        // Outputs
        .GPIO_IN  (  ),
        // Inouts
        .P9_41    ( P9_41 ),
        .P9_42    ( P9_42 )
        );

//--------motors
motors motors_0(                // 
        .clk     ( PCLK ),      // 
        .resetn  ( PRESETN ),   // 
        .motor   ( MOTOR )      // 
        );

endmodule

//wire           PCLK;
//wire           PRESETN;
//wire           MOTOR;
//wire   [31:0]  APB_SLAVE_PRDATA_net_0;
//wire   [27:0]  GPIO_IN_net_1;

//--------------------------------------------------------------------
// Concatenation assignments
//--------------------------------------------------------------------
//assign GPIO_OE_net_0  = { 16'h0000 , GPIO_OE[27:6], 1'b1, GPIO_OE[4:0] };
//assign GPIO_OUT_net_0 = { 16'h0000 , GPIO_OUT[27:6], MOTOR, GPIO_OUT[4:0] };

